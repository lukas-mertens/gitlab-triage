require_relative 'base_query_param_builder'

module Gitlab
  module Triage
    module APIQueryBuilders
      class MultiQueryParamBuilder < BaseQueryParamBuilder
        attr_reader :separator

        def initialize(param_name, param_contents, separator)
          @separator = separator
          super(param_name, param_contents)
        end

        def param_content
          param_contents.map(&:strip).join(separator)
        end
      end
    end
  end
end
