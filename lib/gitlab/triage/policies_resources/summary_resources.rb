# frozen_string_literal: true

module Gitlab
  module Triage
    module PoliciesResources
      class SummaryResources
        attr_reader :rule_to_resources

        def initialize(new_rule_to_resources)
          @rule_to_resources = new_rule_to_resources
        end

        def build_issues
          rule_to_resources.map do |inner_policy_spec, inner_resources|
            yield(inner_policy_spec, inner_resources)
          end
        end
      end
    end
  end
end
