require_relative 'base_command_builder'

module Gitlab
  module Triage
    module CommandBuilders
      class LabelCommandBuilder < BaseCommandBuilder
        private

        def slash_command_string
          "/label"
        end

        def format_item(item)
          "~\"#{item}\""
        end
      end
    end
  end
end
