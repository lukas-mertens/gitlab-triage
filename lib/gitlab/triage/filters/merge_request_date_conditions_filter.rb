require_relative 'issuable_date_conditions_filter'

module Gitlab
  module Triage
    module Filters
      class MergeRequestDateConditionsFilter < IssuableDateConditionsFilter
        ATTRIBUTES = %w[updated_at created_at merged_at].freeze

        # Guard against merge requests with no merged_at values
        def resource_value
          super if @resource[@attribute]
        end

        # Guard against merge requests with no merged_at values
        def calculate
          return false unless resource_value

          super
        end
      end
    end
  end
end
