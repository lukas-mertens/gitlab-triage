require 'spec_helper'

require 'gitlab/triage/command_builders/label_command_builder'

describe Gitlab::Triage::CommandBuilders::LabelCommandBuilder do
  let(:labels) do
    ["bug", "feature proposal"]
  end

  describe '#build_command' do
    it 'outputs the correct command' do
      builder = described_class.new(labels)
      expect(builder.build_command).to eq("/label ~\"bug\" ~\"feature proposal\"")
    end

    it 'outputs an empty string if no labels present' do
      builder = described_class.new([])
      expect(builder.build_command).to eq("")
    end
  end
end
