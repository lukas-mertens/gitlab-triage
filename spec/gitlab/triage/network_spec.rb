require 'spec_helper'

require 'gitlab/triage/options'
require 'gitlab/triage/network'

describe Gitlab::Triage::Network do
  let(:token) { 'token' }
  let(:network_adapter) { double(options: network_options) }

  let(:headers) do
    {
      'Content-type' => 'application/json',
      'PRIVATE-TOKEN' => token
    }
  end

  let(:network_options) do
    options = Gitlab::Triage::Options.new
    options.token = token
    options
  end

  subject { described_class.new(network_adapter) }

  before do
    allow(subject).to receive(:print)
  end

  describe '#query_api' do
    let(:results) { [{ a: 'b' }, { c: 'd' }] }
    let(:ratelimit_remaining) { 600 }
    let(:response) do
      {
        results: results,
        more_pages: false,
        next_page_url: '',
        ratelimit_remaining: ratelimit_remaining,
        ratelimit_reset_at: Time.now + 2
      }
    end
    let(:query_url) { 'https://gitlab.com/api/v4/projects/project/issues' }

    it 'acquires the correct results' do
      expect(network_adapter).to receive(:get).with(token, query_url).and_return(response)
      expect(subject.query_api(query_url)).to eq(results.map(&:with_indifferent_access))
    end

    context 'when the adapter raises a Net::ReadTimeout' do
      it 'returns an empty array' do
        allow(network_adapter).to receive(:get).with(token, query_url).and_raise(Net::ReadTimeout)

        expect(subject.query_api(query_url)).to eq([])
      end
    end

    context 'when the Rate Limit remaining is less than 25' do
      let(:ratelimit_remaining) { 24 }

      it 'waits until the Rate limit is reset' do
        allow(network_adapter).to receive(:get).with(token, query_url).and_return(response)
        expect(subject).to receive(:sleep).with(1).at_least(:twice)

        expect(subject.query_api(query_url)).to eq(results.map(&:with_indifferent_access))
      end
    end
  end

  describe '#query_api_cached' do
    before do
      %w[url_a url_b].each do |url|
        allow(subject).to receive(:query_api).with(url)
          .and_return("URL: #{url}")
      end
    end

    it 'caches whatever #query_api was returning based on url' do
      %w[url_a url_b].each do |url|
        2.times do
          response = subject.query_api_cached(url)

          expect(response).to eq("URL: #{url}")
        end

        expect(subject).to have_received(:query_api).with(url).once
      end
    end
  end

  describe '#post_api' do
    let(:results) { { a: 'b' } }
    let(:ratelimit_remaining) { 600 }
    let(:response) do
      {
        results: results,
        ratelimit_remaining: ratelimit_remaining,
        ratelimit_reset_at: Time.now + 2
      }
    end
    let(:post_url) { 'https://gitlab.com/v4/api/projects/project/issues' }
    let(:body) { 'body' }

    it 'calls the library' do
      expect(network_adapter).to receive(:post).with(token, post_url, body).and_return(response)

      expect(subject.post_api(post_url, body)).to eq(results.with_indifferent_access)
    end

    context 'when the adapter raises a Net::ReadTimeout' do
      it 'returns {}' do
        allow(network_adapter).to receive(:post).with(token, post_url, body).and_raise(Net::ReadTimeout)

        expect(subject.post_api(post_url, body)).to eq({})
      end
    end

    context 'when the Rate Limit remaining is less than 25' do
      let(:ratelimit_remaining) { 24 }

      it 'waits until the Rate limit is reset' do
        allow(network_adapter).to receive(:post).with(token, post_url, body).and_return(response)
        expect(subject).to receive(:sleep).with(1).at_least(:twice)

        expect(subject.post_api(post_url, body)).to eq(results.with_indifferent_access)
      end
    end
  end
end
