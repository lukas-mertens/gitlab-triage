# frozen_string_literal: true

require 'spec_helper'

describe 'summary policy with expansion' do
  include_context 'with integration context'

  let(:feature_issue) do
    issue.merge(labels: %w[feature], iid: issue[:iid] * 2)
  end

  before do
    stub_api(
      :get,
      "https://gitlab.com/api/v4/projects/#{project_id}/issues",
      query: { per_page: 100, labels: 'bug' },
      headers: { 'PRIVATE-TOKEN' => token }) do
      [issue]
    end

    stub_api(
      :get,
      "https://gitlab.com/api/v4/projects/#{project_id}/issues",
      query: { per_page: 100, labels: 'feature' },
      headers: { 'PRIVATE-TOKEN' => token }) do
      [feature_issue]
    end
  end

  it 'creates a grand summary' do
    rule = <<~YAML
      resource_rules:
        issues:
          summaries:
            - name: Grand summary
              actions:
                summarize:
                  title: Grand summary for {{type}}
                  summary: |
                    Grand summary for following {{type}}:

                    {{items}}

              rules:
                - name: Child summary
                  conditions:
                    labels:
                      - |
                        {
                          bug,
                          feature
                        }
                  actions:
                    summarize:
                      item: '- \#{resource[:iid]} {{labels}}'
                      summary: |
                        Child summary for following {{type}}:

                        {{items}}
    YAML

    title = 'Grand summary for issues'
    summary = <<~MARKDOWN.chomp
      Grand summary for following issues:

      Child summary for following issues:

      - #{issue[:iid]} ~"bug"

      Child summary for following issues:

      - #{feature_issue[:iid]} ~"feature"
    MARKDOWN

    stub_post_summary = stub_api(
      :post,
      "https://gitlab.com/api/v4/projects/#{project_id}/issues",
      body: { title: title, description: summary },
      headers: { 'PRIVATE-TOKEN' => token })

    perform(rule)

    assert_requested(stub_post_summary)
  end
end
